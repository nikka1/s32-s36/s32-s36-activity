const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our front-end application
// Allows us to control the app's Cross Origin Resource sharing settings
const cors = require("cors");

const port = 4000;

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@cluster0.jtm9g.mongodb.net/S32-S36?retryWrites=true&w=majority", {

		useNewUrlParser: true,
		useUnifiedTopology: true

});

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

// Allows all resources to access our backend our application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 is none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});