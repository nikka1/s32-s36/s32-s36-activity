const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");


// Route for checking if email exists
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Routes for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


//S33 Activity - Route to retrieve user details
// The "auth.verify" will act as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Enrolling a user
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == false) {
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	}
	else {
		res.send("Admins cannot enroll")
	}
})

module.exports = router;
