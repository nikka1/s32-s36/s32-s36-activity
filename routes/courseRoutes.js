const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");

const auth = require("../auth");



// Route for create a course
/*
router.post("/", (req, res) => {

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;
*/

router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	courseController.addCourse({isAdmin: userData.isAdmin}, req.body).then(resultFromController => res.send(resultFromController))
})


// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res)  => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Mini activity
/*
	Create a route and controller for retrieving all active courses.
	No need to login.

	Use getAllActive as function
	Use "/" as endpoint
*/

// Route for retrieving all active courses
router.get("/", (req, res)  => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController));
})

// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	
	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;